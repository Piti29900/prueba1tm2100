public class Equipo{
    
    private String placa;
    private String marca;
    
    public void setPlaca(String placa){
	
	this.placa=placa;
    }//fin del setPlaca
    
    public String getPlaca(){
	return placa;
    }//fin del getPlaca
    
    public void setMarca(String marca){
	
	this.marca=marca;
    }//fin del setMarca
    
    public String getMarca(){
	return marca;
    }//fin del getMarca


}//fin de la clase 
